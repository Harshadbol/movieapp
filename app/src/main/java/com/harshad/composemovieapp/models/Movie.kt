package com.harshad.composemovieapp.models

data class Movie(
    val id: String,
    val title: String,
    val year: String,
    val genre: String,
    val director: String,
    val actors: String,
    val plot: String,
    val poster: String,
    val rating: String
)

fun getMovies(): List<Movie> {
    val movies = mutableListOf<Movie>()
    for (i in 1..10) {
        if (i % 2 == 0) {
            val samBahadur = Movie(
                "tt0$i",
                "Sam Bahadur",
                "2023",
                "Biography, Drama",
                "Meghna Gulzar",
                "Vicky Kaushal, Sanya Malhotra, Fatima Sana Shaikh, Mohd. Zeeshan Ayyub, Neeraj Kabi, Naoya Ishida, Edward Sonnenblick, Jaskaran Singh Gandhi, Richard Bhakti Klein, Keita Arai, Col Ravi Sharma, Sammy Jonas Heaney, Krishnakant Singh Bundela, Jeffrey Goldberg, Rohan Verma, Ed Robinson, Rajiv Kachroo, Upen Chauhan, Richard Maddison, Nabjot Kaur Tiwana, Paul O'Neill",
                "RSVP Movies",
                "https://i.gadgets360cdn.com/products/large/Sam-Bahadur-640x800-1699944732441.jpg?downsize=*:420",
                "8.3/10"
            )
            movies.add(samBahadur)
        } else {
            val archies = Movie(
                "tt0$i",
                "The Archies",
                "2023",
                "Comedy, Musical, Romance",
                "Zoya Akhtar",
                "Mihir Ahuja, Dot, Khushi Kapoor, Suhana Khan, Yuvraj Menda, Agastya Nanda, Vedang Raina",
                "Tiger Baby Films, Archie Comics, Graphic India",
                "https://i.gadgets360cdn.com/products/large/the-archies-poster-full-4104x7296-1653042856011.jpg?downsize=*:420",
                "2.2/10"
            )
            movies.add(archies)
        }
    }
    return movies
}
