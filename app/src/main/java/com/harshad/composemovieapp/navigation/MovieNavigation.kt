package com.harshad.composemovieapp.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.harshad.composemovieapp.screens.details.DetailsScreen
import com.harshad.composemovieapp.screens.home.HomeScreen

@Composable
fun MovieNavigation() {

    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = MovieScreens.HomeScreen.name) {
        composable(MovieScreens.HomeScreen.name) {
            //here we pass where this should lead us to
            HomeScreen(navController = navController)
        }

        //pass string argument like this /id=55
        composable(
            MovieScreens.DetailsScreen.name + "/{movie}",
            arguments = listOf(navArgument(name = "movie") { type = NavType.StringType })
        ) {backStackEntry->
            DetailsScreen(navController = navController,backStackEntry.arguments?.getString("movie"))
        }
    }
}