package com.harshad.composemovieapp.screens.home

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.harshad.composemovieapp.models.Movie
import com.harshad.composemovieapp.models.getMovies
import com.harshad.composemovieapp.navigation.MovieScreens
import com.harshad.composemovieapp.widgets.MovieRow


@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeScreen(navController: NavController) {
    Scaffold(topBar = {
        TopAppBar(
            title = { Text(text = "Movie") },
            colors = TopAppBarDefaults.smallTopAppBarColors(containerColor = Color.LightGray)
        )
    }) {
        MainContent(navController = navController)
    }
}

@Composable
fun MainContent(
    navController: NavController,
    movieList: List<Movie> = getMovies()
) {
    Surface(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
    ) {
        Column(
            modifier = Modifier.padding(
                top = 70.dp,
                start = 12.dp,
                end = 12.dp,
                bottom = 12.dp
            )
        ) {
            LazyColumn {
                items(items = movieList) { movie ->
                    MovieRow(movie = movie) { movieId ->
                        navController.navigate(route = MovieScreens.DetailsScreen.name + "/$movieId")
                    }
                }
            }
        }
    }
}

